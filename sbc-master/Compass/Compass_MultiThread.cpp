/**
 * Developed for the DRDC Radio Frequency Measurement System.
 * This product includes software developed by William Woodall.
 * 
 * Purpose: Tests reading and writing to a serial device with multithreading
 * 
 *  @author Nicholas Wilkie
 *  @version 2020-03-31
 */
#include "serial/serial.h"
#include <iostream>
#include <string>
#include <thread>
#include <chrono>

using namespace std::this_thread; // sleep_for, sleep_until
using namespace std;
using namespace serial;

void writeCompass()
{
    sleep_for(1000ms);
    uint32_t baudrate = 19200;
    string output = "$PTNT,HTM*63";
    const std::string &port = "/dev/ttyUSB0";
    const std::string &data = "$PTNT,HTM*63\r\n";
    bytesize_t bytesize = eightbits;
    parity_t parity = parity_none;
    stopbits_t stopbits = stopbits_one;
    flowcontrol_t flowcontrol = flowcontrol_none;
    Timeout timeout = Timeout();
    size_t dataWrite;

    serial::Serial myCompass2(port, baudrate, timeout, bytesize, parity, stopbits, flowcontrol);

    cout << "\nWrite Data!\n";
    dataWrite = myCompass2.write(data);
    cout << dataWrite;
    cout << "\n";
}

void readCompass()
{
    int32_t baudrate = 19200;
    string port = "/dev/ttyUSB0";
    bytesize_t bytesize = eightbits;
    parity_t parity = parity_none;
    stopbits_t stopbits = stopbits_one;
    flowcontrol_t flowcontrol = flowcontrol_none;
    //Timeout timeout = Timeout();
    size_t size = 65536;
    string delim = "\n";
    serial::Serial myCompass(port, baudrate, serial::Timeout::simpleTimeout(10000), bytesize, parity, stopbits, flowcontrol);
    string lineIn = myCompass.readline(size, delim);
    cout << lineIn;
    cout << "\nDoes it print out? \n";
}

int main()
{

    void (*readfunction)();
    void (*writefunction)();
    readfunction = readCompass;
    writefunction = writeCompass;

    std::thread t1(readfunction);
    std::thread t2(writefunction);
    //port, baudrate, timeout, bytesize, parity, stopbits, flowcontrol
    t1.join();
    t2.join();
}