/**
 * Developed for the DRDC Radio Frequency Measurement System.
 * This product includes software developed by William Woodall.
 *
 * Purpose: Reads, stores, and returns NMEA 0183 data from the True North
 * Technologies Revolution Compass.
 *
 *  @author Nicholas Wilkie
 *  @version 2020-03-30
 */
#include "CompassIO.h"

// The default constructor.
CompassIO::CompassIO()
{
    baudRate = 19200;
    byteSize = eightbits;
    port = "/dev/ttyUSB0";
    data = "$PTNT,HTM*63\r\n";
    timeout = serial::Timeout::simpleTimeout(10000);
    parity = parity_none;
    stopBits = stopbits_one;
    flowControl = flowcontrol_none;
    size = 65536;
    eol = "\n";
    dataIn = "";
    delim = ',';
}

/**
 * Overload of the default constructor allowing configuration of serial device
 * settings.
 * @param baudRateIn A unsigned 32-bit integer representing the baud rate of the
 * serial device
 * @param byteSizeIn Size of each byte in the serial transmission of data,
 * default is eightbits, possible values are: fivebits, sixbits, sevenbits,
 * eightbit
 * @param portIn a string containing the address of the serial port ex. "COM1"
 * on windows "/dev/ttyUSB0" on unix
 * @param dataIn a string of the data being written to the serial port
 * @param timeoutIn a serial::Timeout struct defining the timeout condition for
 * the serial port
 * @param parityIn Method of parity, default is parity_none, possible values
 * are: parity_none, parity_odd, parity_even
 * @param stopBitsIn Number of stop bits used, default is stopbits_one, possible
 * values are: stopbits_one, stopbits_one_point_five, stopbits_two
 * @param flowControlIn Type of flowcontrol used, default is flowcontrol_none,
 * possible values are: flowcontrol_none, flowcontrol_software,
 * flowcontrol_hardware
 * @param sizeIn unsigned long defining the max line length of data being read
 * from the serial device
 * @param eolIn a string to match against the EOL when reading data from the
 * serial device
 */
CompassIO::CompassIO(uint32_t baudRateIn, bytesize_t byteSizeIn, string portIn,
                     string dataIn, Timeout timeoutIn, parity_t parityIn,
                     stopbits_t stopBitsIn, flowcontrol_t flowControlIn,
                     size_t sizeIn, string eolIn)
{
    baudRate = baudRateIn;
    byteSize = byteSizeIn;
    port = portIn;
    data = dataIn;
    timeout = timeoutIn;
    parity = parityIn;
    stopBits = stopBitsIn;
    flowControl = flowControlIn;
    size = sizeIn;
    eol = eolIn;
    dataIn = "";
    delim = ',';
}

/**
 * Reads in one line of data from the compass and stores it in a string vector.
 * Requires a simultanious write to the compass of $PTNT,HTM*63.
 * The format of the read in data is $PTNTHTM,39.0,O,17.2,N,9.8,N,64.2,4499*3F
 */
void CompassIO::read()
{
    // Declares a serial object with the serial devices parameters.
    serial::Serial myCompass(port, baudRate, timeout, byteSize, parity, stopBits,
                             flowControl);
    int length;
    int start;
    int end;

    // Checks status of device and opens it if it is no already.
    if (!myCompass.isOpen())
    {
        cout << "The port is not open, opening...";
        myCompass.open();
    }

    // Starts the reading process. This function will timeout if no input is
    // recieved within the specificed timeout variable time.
    dataIn = myCompass.readline(size, eol);

    length = dataIn.length() - 1;
    start = 0;
    end = 0;
    compassData.clear(); // empties the string vector
    //int lastDelim;
    // splits the input data into sections that are stored in the string vector
    // based on a delimiter
    for (int i = 0; i < 10; i++)
    {
        // searches for the location of the delimiter in the dataIn string
        end = dataIn.find_first_of(delim, start);
        if (end < length)
        {
            // searches for the location of the delimiter in the dataIn string
            compassData.push_back(dataIn.substr(start, end - start));
        }
        start = end + 1;
    }
    //lastDelim = dataIn.find_last_of(delim, length);
    //compassData.push_back(dataIn.substr(lastDelim + 1, dataIn.length() - lastDelim));
    //myCompass.close();
}

void CompassIO::write()
{
    serial::Serial myCompass2(port, baudRate, timeout, byteSize, parity, stopBits,
                              flowControl);
    myCompass2.write(data);
}

//attempt at multithreading to deal with compass full duplex input (NOT WORKING)
void CompassIO::recordData()
{

    //void (*readfunction)();
    //void (*writefunction)();
    //readfunction = &CompassIO::read;
    //writefunction = write;
    //
    //std::thread t1(readfunction);
    //std::thread t2(writefunction);
    ////port, baudrate, timeout, bytesize, parity, stopbits, flowcontrol
    //t1.join();
    //t2.join();
}
/**
 * @return the vector of the compass data set
 */
vector<string> CompassIO::getCompassData() { return (compassData); }

/**
 * @return the string output from the compass
 */
string CompassIO::getDataReadIn() { return (dataIn); }

/**
 * @return A double of the true heading number in degrees
 */
double CompassIO::getHeadingNum() { return (atof(compassData[1].c_str())); }

/**
 * @return A string of the true heading number in degrees
 */
string CompassIO::getHeadingString() { return (compassData[1]); }

/**
 * @return A string of the magnetometer status character
 */
string CompassIO::getMagnStatus() { return (compassData[2]); }

/**
 * @return A string of the magnetometer status descriptor
 */
string CompassIO::getMagnStatusText() { return (statusCheck(compassData[2])); }

/**
 * @return A double of the pitch angle
 */
double CompassIO::getPitchNum() { return (atof(compassData[3].c_str())); }

/**
 * @return A string of the pitch angle
 */
string CompassIO::getPitchString() { return (compassData[3]); }

/**
 * @return A string of the pitch status character
 */
string CompassIO::getPitchStatus() { return (compassData[4]); }

/**
 * @return A string of the pitch status descriptor
 */
string CompassIO::getPitchStatusText() { return (statusCheck(compassData[4])); }

/**
 * @return A double of the roll angle
 */
double CompassIO::getRollNum() { return (atof(compassData[5].c_str())); }

/**
 * @return A string of the roll angle
 */
string CompassIO::getRollString() { return (compassData[5]); }

/**
 * @return A string of the roll status character
 */
string CompassIO::getRollStatus() { return (compassData[6]); }

/**
 * @return A string of the roll status descriptor
 */
string CompassIO::getRollStatusText() { return (statusCheck(compassData[6])); }

/**
 * @return A double of the dip angle
 */
double CompassIO::getDipNum() { return (atof(compassData[7].c_str())); }

/**
 * @return A string of the dip angle
 */
string CompassIO::getDipString() { return (compassData[7]); }

/**
 * @return A string of the relative magnitude horizontal component of earth’s
 * magnetic field
 */
string CompassIO::getHorizMagn() { return (compassData[8]); }

/**
 * Provides the descriptor for a compass status character.
 * @param letterIn The status character
 * @return the text description of a compass status character
 */
string CompassIO::statusCheck(char letterIn)
{
    switch (letterIn)
    {
    case 'C':
        return ("Magnetometer Calibration Alarm");
        break;
    case 'L':
        return ("Low Alarm");
        break;
    case 'M':
        return ("Low Warning");
        break;
    case 'N':
        return ("Normal");
        break;
    case 'O':
        return ("High Warning");
        break;
    case 'P':
        return ("High Alarm");
        break;
    case 'H':
        return ("Magnetometer Voltage Level Alarm");
        break;
    }
}

/**
 * Overload of statusCheck function to allow string input.
 * @param letterIn The status character
 * @return the text description of a compass status character
 */
string CompassIO::statusCheck(string letterIn)
{
    char letter = letterIn[0];
    return (statusCheck(letter));
}