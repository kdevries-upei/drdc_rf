/**
 * Developed for the DRDC Radio Frequency Measurement System.
 * This product includes software developed by William Woodall.
 * 
 * 
 *  @author Nicholas Wilkie
 *  @version 2020-03-30
 */
#include <iostream>
#include <string>
#include <vector>
#include "serial/serial.h"
using namespace std;
using namespace serial;

#ifdef COMPASSIO_H
#endif
#ifndef COMPASSIO_H
class CompassIO
{
private:
    uint32_t baudRate;
    string port;
    string data;
    bytesize_t byteSize;
    parity_t parity;
    stopbits_t stopBits;
    flowcontrol_t flowControl;
    Timeout timeout;
    size_t size;
    string dataIn;
    string eol;
    char delim;
    vector<string> compassData;

public:
    CompassIO();
    CompassIO(uint32_t baudRateIn, bytesize_t byteSizeIn, string portIn, string dataIn, Timeout timeoutIn, parity_t parityIn, stopbits_t stopBitsIn, flowcontrol_t flowControlIn, size_t sizeIn, string eolIn);
    void read();
    void write();
    void recordData();
    vector<string> getCompassData();
    string getDataReadIn();
    double getHeadingNum();
    string getHeadingString();
    string getMagnStatus();
    string getMagnStatusText();
    double getPitchNum();
    string getPitchString();
    string getPitchStatus();
    string getPitchStatusText();
    double getRollNum();
    string getRollString();
    string getRollStatus();
    string getRollStatusText();
    double getDipNum();
    string getDipString();
    string getHorizMagn();
    string statusCheck(char letterIn);
    string statusCheck(string letterIn);
};
#endif
