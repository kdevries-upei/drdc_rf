/**
 * Developed for the DRDC Radio Frequency Measurement System.
 * This product includes software developed by William Woodall.
 * 
 *  @author Nicholas Wilkie
 *  @version 2020-03-31
 */
#include <iostream>
#include <string>
#include <vector>
#include "serial/serial.h"
using namespace std;
using namespace serial;

class gpsIO
{
private:
    uint32_t baudRate;
    string port;
    bytesize_t byteSize;
    parity_t parity;
    stopbits_t stopBits;
    flowcontrol_t flowControl;
    Timeout timeout;
    size_t size;
    string dataIn;
    string eol;
    char delim;
    int satelliteFixes;
    vector<string> gpsData;

public:
    gpsIO();
    gpsIO(uint32_t baudRateIn, bytesize_t byteSizeIn, string portIn, Timeout timeoutIn, parity_t parityIn, stopbits_t stopBitsIn, flowcontrol_t flowControlIn, size_t sizeIn, string eolIn, int satelliteNumber);
    bool readOnce();
    void readUntilFix();
    vector<string> getCompassData();
    string getDataReadIn();
    double getTimeNum();
    string getTimeString();
    double getLatitudeNum();
    string getLatitudeString();
    string getLatDirec();
    double getLongitudeNum();
    string getLongitudeString();
    string getLonDirec();
    double getFixNum();
    string getFixString();
    int getSatelliteNum();
    string getAltitudeString();
    double getAltitudeNum();
    string getHorDilutionString();
    double getHorDilutionNum();
    string geoidHeightString();
    double geoidHeighNum();
    string checkSumData();
};