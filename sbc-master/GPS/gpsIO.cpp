/**
 * Developed for the DRDC Radio Frequency Measurement System.
 * This product includes software developed by William Woodall.
 * 
 * Purpose: Reads, stores, and returns gps data from the GlobalSat BU-353S4 GPS.
 * 
 *  @author Nicholas Wilkie
 *  @version 2020-03-31
 */
#include <chrono>
#include <thread>
#include "gpsIO.h"

using namespace std::this_thread;
using namespace std::chrono;

/**
 * Default constructor
*/
gpsIO::gpsIO()
{
  baudRate = 4800;
  byteSize = eightbits;
  port = "/dev/ttyUSB0";
  timeout = serial::Timeout::simpleTimeout(1000);
  parity = parity_none;
  stopBits = stopbits_one;
  flowControl = flowcontrol_none;
  size = 65536;
  dataIn = "";
  delim = ',';
  eol = "\n";
  satelliteFixes = 8;
}

/**
 * Default constructor overload allowing configuration of serial device(GPS) parameters 
 * and the required satellites being tracked before the gps location is recorded.
 * @param baudRateIn A unsigned 32-bit integer representing the baud rate of the serial device
 * @param byteSizeIn Size of each byte in the serial transmission of data, default is eightbits, possible values are: fivebits, sixbits, sevenbits, eightbit
 * @param portIn a string containing the address of the serial port ex. "COM1" on windows "/dev/ttyUSB0" on unix
 * @param dataIn a string of the data being written to the serial port
 * @param timeoutIn a serial::Timeout struct defining the timeout condition for the serial port
 * @param parityIn Method of parity, default is parity_none, possible values are: parity_none, parity_odd, parity_even
 * @param stopBitsIn Number of stop bits used, default is stopbits_one, possible values are: stopbits_one, stopbits_one_point_five, stopbits_two
 * @param flowControlIn Type of flowcontrol used, default is flowcontrol_none, possible values are: flowcontrol_none, flowcontrol_software, flowcontrol_hardware
 * @param sizeIn unsigned long defining the max line length of data being read from the serial device
 * @param eolIn a string to match against the EOL when reading data from the serial device
 * @param satelliteNumber an integer specifying the number of satellites the GPS has to be tracking before it will store the GPS data
*/
gpsIO::gpsIO(uint32_t baudRateIn, bytesize_t byteSizeIn, string portIn, Timeout timeoutIn, parity_t parityIn,
             stopbits_t stopBitsIn, flowcontrol_t flowControlIn, size_t sizeIn,
             string eolIn, int satelliteNumber)
{
  baudRate = baudRateIn;
  byteSize = byteSizeIn;
  port = portIn;
  timeout = timeoutIn;
  parity = parityIn;
  stopBits = stopBitsIn;
  flowControl = flowControlIn;
  size = sizeIn;
  eol = eolIn;
  dataIn = "";
  satelliteFixes = satelliteNumber;
}

/**
 * Creates a serial device object and reads in one line of data from the serial device.
 * The data read in is split into parts and stored in the string vector.
 * @return a boolean to indicate if the data recieved from the GPS was readable
*/
bool gpsIO::readOnce()
{
  serial::Serial myGPS(port, baudRate, timeout, byteSize, parity, stopBits,
                       flowControl);
  int gpsQuality = 0;
  int start;
  int end;
  int length;
  //int lastDelim;

  if (!myGPS.isOpen())
  {
    cout << "The port is not open, opening...";
    myGPS.open();
  }

  dataIn = myGPS.readline(size, eol);

  length = dataIn.length() - 1;

  //cout << dataIn;
  gpsData.clear();
  start = 0;
  end = 0;

  //Checks if the sentence read from the serial device is complete.
  if (!dataIn.substr(0, 6).compare("$GPGGA"))
  {
    //splits the input data into sections that are stored in the string vector based on a delimiter
    for (int i = 0; i < 13; i++)
    {
      //searches for the location of the delimiter in the dataIn string
      end = dataIn.find_first_of(delim, start);
      if (end < length)
      {
        //inputs data into the string vector
        gpsData.push_back(dataIn.substr(start, end - start));
      }
      start = end + 1;
    }
    //lastDelim = dataIn.find_last_of(delim, length);
    //gpsData.push_back(dataIn.substr(lastDelim + 1, dataIn.length() - lastDelim));
    return (true);
  }
  else
  {
    //returned if an incomplete sentence was read from the serial device
    return (false);
  }
}

/**
 * Calls the read function until the specified number of satellites are being tracked by the GPS.
*/
void gpsIO::readUntilFix()
{
  int gpsQuality = 0;
  while (gpsQuality < satelliteFixes)
  {
    if (readOnce())
    {
      gpsQuality = getSatelliteNum();
    }
  }
}

/**
 *@return A string of the sentence read from the serial device (GPS)
*/
string gpsIO::getDataReadIn() { return (dataIn); }

/**
 *@return A double of the time (UTC)
*/
double gpsIO::getTimeNum() { return (atof(gpsData[1].c_str())); }

/**
 *@return A string of the time (UTC)
*/
string gpsIO::getTimeString() { return (gpsData[1]); }

/**
 *@return A double of the latitude of the GPS
*/
double gpsIO::getLatitudeNum() { return (atof(gpsData[2].c_str())); }

/**
 *@return A string of the latitude of the GPS
*/
string gpsIO::getLatitudeString() { return (gpsData[2]); }

/**
 *@return A string of the latitude direction (N or S) of the GPS
*/
string gpsIO::getLatDirec() { return (gpsData[3]); }

/**
 *@return A double of the longitude of the GPS
*/
double gpsIO::getLongitudeNum() { return (atof(gpsData[4].c_str())); }

/**
 *@return A string of the longitude of the GPS
*/
string gpsIO::getLongitudeString() { return (gpsData[4]); }

/**
 *@return A string of the longitude direction (W or E) of the GPS
*/
string gpsIO::getLonDirec() { return (gpsData[5]); }

/**
 *@return A double of the fix quality of the GPS
*/
double gpsIO::getFixNum() { return (atof(gpsData[6].c_str())); }

/**
 *@return A string of the fix quality of the GPS
*/
string gpsIO::getFixString() { return (gpsData[6]); }

/**
 *@return An integer of the number of satellites being tracked by the GPS
*/
int gpsIO::getSatelliteNum() { return (atoi(gpsData[7].c_str())); }

/**
 *@return A string of the horizontal dilution of position
*/
string gpsIO::getHorDilutionString() { return (gpsData[8]); }

/**
 *@return A double of the horizontal dilution of position
*/
double gpsIO::getHorDilutionNum() { return (atof(gpsData[8].c_str())); }

/**
 *@return A string of the altitude above mean sea level in meters
*/
string gpsIO::getAltitudeString() { return (gpsData[9]); }

/**
 *@return A double of the altitude above mean sea level in meters
*/
double gpsIO::getAltitudeNum() { return (atof(gpsData[9].c_str())); }

/**
 *@return A string of the Height of geoid (mean sea level) above WGS84 ellipsoid
*/
string gpsIO::geoidHeightString() { return (gpsData[11]); }

/**
 *@return A double of the Height of geoid (mean sea level) above WGS84 ellipsoid
*/
double gpsIO::geoidHeighNum() { return (atof(gpsData[11].c_str())); }

/**
 *@return A string of the checksum data of the GPS device
*/
string gpsIO::checkSumData() { return (gpsData[12]); }
