/*
 *
 * FILE INFO GOES HERE
 *
 */

#include <fcntl.h>		// File IO functions
#include "functionDeclarations.h"
#include <iostream>		// Stream IO functions
#include <mutex>		// Thread-safe variable communication
//#include <ofstream>		// Logging functions
#include "plog/Log.h"
#include <stdio.h>		//
#include <string.h>		// String processing
#include <termios.h>	// Serial communication
#include <thread>		// Multithreading capabilities
#include <unistd.h>		// Standard system functions (usleep, etc.)

#include "serial/serial.h"
#include <chrono>


#define SSDPATH "________"		// **** REPLACE WITH ACTUAL SSD PATH ****


// Serial parameters
#define BAUDRATE B115200		// Transmission rate
#define MODEMDEV "/dev/ttymxc2"	// UART3 device node (name indexing starts at 0)
#define POSIXSRC 1				// Indicates that source complies with POSIX standards //XXXXX does it?
#define STRLNGTH 255			// Max length of string read from serial port

// Serial command values
//#define START_CMD "{"		// Begin serial transmission
//#define START_RPL "}"		// Confirmation of START_CMD received
//#define CONT_CMD  "0"		// Continue serial transmission
//#define PAUSE_CMD "1"		// Pause serial transmission
//#define STOP_CMD  "2"		// End serial transmission
//#define SHUTDOWN  "3"		// System Shutdown

// Reserved characters
#define PLC_BOL '<'			// PLC's beginning-of-line character
#define PLC_DEL '\\'		// PLC's delimiter character (single backslash)

#define TIMEOUT_VAL	100000	// readSerial timeout value (microseconds)
#define TIMEOUT_LIM 10		// Max number of consecutive timeouts from reading serial port
							//  - Likely indicates failed connection

using namespace std;
using namespace serial;
using namespace std::this_thread; // sleep_for, sleep_until


int shutdown = 0;
int getGPS = 0;


// Thread locks - prevents multiple threads from accessing same memory simultaneously 
std::mutex gpsLock;
std::mutex compassLock;


struct
{
	std::string latStr;
	std::string longStr;
	std::string altStr;
	std::string timeStr;
} gpsDataTemp;


struct
{
	//
} compassDataTemp;



class ThreadFxns
{
public:
	ThreadFxns();
	int ThreadRun();

private:
	void comm();
	void compass();
	void gps();
	void logs();
	void radio();
};


// Function Prototypes
int readSerial(int deviceNode, char buffer[STRLNGTH], int timeout = TIMEOUT_VAL);
int writeSerial(int deviceNode, char buffer[STRLNGTH]);







// ========== Master Control ==========
//  - Starts when system turns on, completes when system turns off
//  - Performs program initialization and shutdown procedures

int main(void)
{

	// Create instance of thread object. Constructor (ThreadFxns::ThreadFxns) automatically called
	ThreadFxns threadObj;

	// Entering primary operating loop
	threadObj.ThreadRun();
	// Operation complete - beginning system shutdown

	//XXXXX Shutdown procedures
	return 0;
}



// ========== Thread Constructor ==========
//  - Automatically called when threadObj created
//  - Performs first-time thread initialization when system powered on
//     - Actions occurring repeatedly (resetting thread control variables, etc.) handled in thread loops

ThreadFxns::ThreadFxns()
{
	//XXXXX Initialize thread elements

}



// ========== Main Operation ==========
//  - Creates threads

int ThreadFxns::ThreadRun()
{
	gpsDataTemp gpsData;
	compassDataTemp compassData;
	std::string fileName;

	// Create threads
	thread thComm(&ThreadFxns::comm, this);
	thread thCompass(&ThreadFxns::compass, this);
	thread thGPS(&ThreadFxns::gps, this);
	thread thLogs(&ThreadFxns::logs, this);
	thread thRadio(&ThreadFxns::rdio, this);

	// Wait for all threads to complete
	thComm.join();
	thCompass.join();
	thGPS.join();
	thLogs.join();
	thRadio.join();

	return 0;
}



// ========== Communication Thread ==========
//  - Opens serial communication line between PLC and SBC
//  - Serial IO based on information here:		https://tldp.org/HOWTO/Serial-Programming-HOWTO/x115.html

void ThreadFxns::comm()
{
	// Serial communications
	struct termios nodeAttr;		// Serial device node attributes
	int serialNode, readVal, s_bufIn, s_bufRef, s_bufOut, n, timeoutVal = 0,
			compass = 60, aoa = 30, inc = 5, azLoop = 1, elLoop = 1, tilt = 5, sig = -20;
	//	char bufInR[STRLNGTH];
	//char bufIn[] = {0x56, 0x45, 0x52, 0x0D, 0x00};//[STRLNGTH];
	//string buffer;
	char bufIn[STRLNGTH];
	char bufOut[STRLNGTH];

	char bufRef[STRLNGTH];

	serialNode = open(MODEMDEV, O_RDWR | O_NOCTTY);
	if (serialNode < 0)
	{
		perror(MODEMDEV);
		shutdown = 1;
	}

    bzero(&nodeAttr, sizeof(nodeAttr)); // Zero struct containing new device attributes

    nodeAttr.c_cflag = BAUDRATE |		// Assigns defined baudrate
    				   CS8 |			// Transmissions are 8N1 (8 character bits, no parity, 1 stop bit)
					   CLOCAL |			// Local connection, no modem control
					   CREAD;// |			// Enable receiving characters
					  //-CRTSCTS;			// Disable hardware flow control

    nodeAttr.c_iflag = ICRNL;			// Convert carriage returns to new lines
    nodeAttr.c_oflag = 0;
    nodeAttr.c_lflag = ICANON;// | IEXTEN;			// Indicates canonical input (inputs processed as lines, waits until EOL char received, e.g. \n)
    //nodeAttr.c_cc[VEOL] = '\n';
    //nodeAttr.c_cc[VEOL2] = '\n';
    //XXXXX Add control characters?

    // Clean the modem line
    tcflush(serialNode, TCIFLUSH);

    // Assign serial attributes to active device
    tcsetattr(serialNode, TCSANOW, &nodeAttr);


	//    cout << "Wrote " << n << " bytes to port\n";


    // Begin main communication
    if (shutdown != 1)
    {
   		s_bufRef = snprintf(bufRef, STRLNGTH, "{");

   		do
    	{
    		// Monitor serial line. Completes when EOL character ("\n") received
    		s_bufIn = read(serialNode, bufIn, STRLNGTH);		// s_bufIn);
    		cout << "Read attempt: >" << bufIn << "<" << endl;

    	} while (!strcmp(bufIn, bufRef));

    	// Send initialization complete message to PLC
    	s_bufOut = snprintf(bufOut, STRLNGTH, "}");
    	n = write(serialNode, bufOut, s_bufOut);
    	tcdrain(serialNode);
    	cout << "Received start character" << endl;

    	while (azLoop)
    	{
    		s_bufOut = snprintf(bufOut, STRLNGTH, "<\\%d\\%d\\", compass, aoa);
    		n = write(serialNode, bufOut, s_bufOut);
    		tcdrain(serialNode);
    		//cout << "<\\" << compass << "\\" << aoa << "\\" << endl;

    		compass += inc;
    		aoa -= inc;

    		s_bufIn = read(serialNode, bufIn, STRLNGTH);		// s_bufIn);
    		s_bufRef = snprintf(bufRef, STRLNGTH, "0");
    		if ( !strcmp(bufIn, bufRef))
    		{
    			cout << "Received 0" << endl;
    		}
    		else
    		{
    			s_bufRef = snprintf(bufRef, STRLNGTH, "1");
    			if ( !strcmp(bufIn, bufRef))
    			{
    				azLoop = 0;
    				cout << "Received 1" << endl;
    			}
    			else
    			{
    				//cout << "Received " << bufIn << endl;
    			}
    		}
    	}

    	// Confirm elevation starting
    	s_bufOut = snprintf(bufOut, STRLNGTH, "{\\");
   		n = write(serialNode, bufOut, s_bufOut);
   		tcdrain(serialNode);
   		cout << "Starting elevation" << endl;

    	while (elLoop)
    	{
    		s_bufOut = snprintf(bufOut, STRLNGTH, "<\\%d\\%d\\", tilt, sig);
    		n = write(serialNode, bufOut, s_bufOut);
    		tcdrain(serialNode);
    		cout << "<\\" << tilt << "\\" << sig << "\\" << endl;

    		tilt += inc;

    		s_bufIn = read(serialNode, bufIn, STRLNGTH);		// s_bufIn);
    		s_bufRef = snprintf(bufRef, STRLNGTH, "0");
    		if ( !strcmp(bufIn, bufRef))
    		{
    			cout << "Received 0" << endl;
    		}
    		else
    		{
    			s_bufRef = snprintf(bufRef, STRLNGTH, "1");
    			if ( !strcmp(bufIn, bufRef))
    			{
    				elLoop = 0;
    				cout << "Received 1" << endl;
    			}
    			else
    			{
    				//cout << "Received " << bufIn << endl;
    			}
    		}

    	}

    	// Request measurement complete
		s_bufIn = read(serialNode, bufIn, STRLNGTH);

		cout << "Received " << bufIn << endl;



    	//    	res = readSerial(serialNode, bufIn, 5000000);

    	// Process received string, extracting actual characters
    	//char x = (*bufIn)[res];// = '\0';
	//    	bufIn[res] = 0; //XXXXX not needed w/ null terminator?
    	cout << "Input: " << bufIn;
    	cout << "Complete\n";
    	// Compare received value against start command
    	//  - strcmp returns 0 for equivalent strings
	//		if (!strcmp(*bufIn, START_CMD)) //XXXXX will buf read null char?
	/*		if (strcmp(bufIn, "{")) //XXXXX ADD INVERTER ! -- will buf read null char?
    	{
			// Send START_RPL
	//			snprintf(*bufIn, STRLNGTH, "%c", START_RPL);
			snprintf(bufIn, STRLNGTH, "%c", '}');
			ssize_t const writeSize {writeSerial(serialNode, bufIn)};
			if (writeSize < 2) //XXXXX
			{
				// Write error, retry? //XXXXX
			}

			do
			{
				// Send compass and AoA string		 //????? And status update/
				writeSerial(serialNode, bufOut);

				// Read serial port for reply
				do
				{
					readVal = readSerial(serialNode, bufOut);

					// Read error
					if (readVal < 0)
						1;//XXXXX goto commLoopError;

					// Timeout
					else if (readVal == 0)
					{
						if (timeoutVal < TIMEOUT_LIM)
							timeoutVal++;
						else
							timeoutVal = 0;
							//XXXXX goto commLoopTimeout;
					}
				} while ((readVal != 0));

	//				switch(bufIn) {

					// Continue transmission
	//					case CONT_CMD:
						//
	//						break;

					// PLC Transmission incoming
	//					case PAUSE_CMD:
	//						readSerial(serialNode, bufOut);
	//						break;

	//					case STOP_CMD:
						//
	//						break;

	//					default:
						//
	//					}
					// ...break

				// if read successful
					// Reset timeout error value

	//			} while (!strcmp(*bufIn, STOP_CMD));
			} while (!strcmp(bufIn, "3"));

			//... received stop command or repeated timeout
    	}

		else
		{

		}			*/
	}

    close(serialNode);
	cout << "Comm thread closed\n";
}



// Data Logging Thread Function
void ThreadFxns::compass()
{
	void (*compass_readfunction)();
    void (*compass_writefunction)();
	void (*gps_readfunction)();
    compass_readfunction = readCompass;
    compass_writefunction = writeCompass;
	gps_readfunction =  gpsOne.readUntilFix();
    std::thread t1(compass_readfunction);
    std::thread t2(compass_writefunction);

    //port, baudrate, timeout, bytesize, parity, stopbits, flowcontrol
    t1.join();
    t2.join();
}



// Data Collection Thread Function
void ThreadFxns::gps()
{
	gpsIO gpsOne = gpsIO();	// Create GPS object
	
	while (shutdown != 1)
	{
		if (getGPS == 1)
		{
			gpsOne.readUntilFix();

			gpsLock.lock();		// This thread now is the only one able to access the gpsData struct

			gpsData.latStr.assign(gpsOne.getLatitudeString()); 
			gpsData.longStr.assign(gpsOne.getLongitudeString()); 
			gpsData.altStr.assign(gpsOne.getAltitudeString());
			gpsData.timeStr.assign(gpsOne.getTimeString());

			gpsLock.unlock();	// Releases gpsData for access by other threads

			getGPS = -1;		// Indicate to comms loop that new gps information is ready
		}
	}
	// cout << "Data loop closed\n";
}



// ========== Data Logging Thread ==========
//  - PLOGI repository here:	https://github.com/SergiusTheBest/plog#step-1-adding-includes
void ThreadFxns::logs()
{
	gpsDataTemp gpsData_Log;
	
	// Get time/date for log file name
	while (getGPS != -1)
	{
		wait(lock);		// Wait until getGPS = -1
	}

	// Safely retrieve initial GPS information and copy to 
	gpsLock.Lock();
	gpsData_Log = gpsData;
	gpsLock.Unlock();

	// Create logging filename, including directory to SSD
	fileName.assign(gpsData_Log.timeStr);
	fileName.insert(0, "LOG-");
	fileName.insert(0, SSDPATH);
	
	// Initialize Logger instance (Max severity level, log name, max file size, max file number)
	plog::init(plog::info, fileName, 1000000, 10);
	PLOGI << "Log header goes here...\there"

	while (shutdown != 1)
	{
		if (getGPS == -1)
		{
			// Update GPS info
			gpsLock.Lock();
			gpsData_Log = gpsData;
			gpsLock.Unlock();
			PLOGI << "\nNEW GPS READING\nTime = " << gpsData_Log.timeStr
				  << "\nLatitude = " << gpsData_Log.latStr
				  << "\nLongitude = " << gpsData_Log.longStr << "\n";
		}
	}
}



// Radio Processing Thread Function
void ThreadFxns::rdio()
{
	// INTEGRATION WITH RADIO PROGRAM HERE

	cout << "Rdio thread closed\n";
}







int readSerial(int deviceNode, char buffer[STRLNGTH], int timeout)
{
	fd_set setStruct;
	FD_ZERO(&setStruct);
	FD_SET(deviceNode, &setStruct);

	struct timeval timeStruct;
	timeStruct.tv_sec = 0;
	if (timeout < 0) timeStruct.tv_usec = 0;
	else timeStruct.tv_usec = timeout;

	int readVal = select(deviceNode + 1, &setStruct, NULL, NULL, &timeStruct);

	if (readVal > 0)
	{
		read(deviceNode, buffer, STRLNGTH);
		cout << buffer << "\n";
	}



	return readVal;
}



int writeSerial(int deviceNode, char buffer[STRLNGTH])
{
//	int s_buffer = snprintf(*buffer, STRLNGTH, "%c%c%s%c%s%c", PLC_BOL, PLC_DEL, 'compass', PLC_DEL, 'angle', PLC_DEL);
	int s_buffer = snprintf(buffer, STRLNGTH, "<\\90\\0\\");//"%c%s%s%s%s%s", '<', '\\', 'compass', '\\', 'angle', '\\');
	write(deviceNode, buffer, s_buffer); //XXXXX need to check write size every time?
	tcdrain(deviceNode);

	return s_buffer;
}