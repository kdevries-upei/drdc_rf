
#include "/home/drdc/drdc_rf/serial-master/CompassIO.h"


using namespace serial;

//base constructor
CompassIO::CompassIO()
{
    baudRate = 19200;
    byteSize = eightbits;
    port = "/dev/ttyUSB0";
    data = "$PTNT,HTM*63";
    timeout = serial::Timeout::simpleTimeout(10000);
    parity = parity_none;
    stopBits = stopbits_one;
    flowControl = flowcontrol_hardware;
    size = 65536;
    eol = "\r\n";
    dataIn ="";
    delim = ',';   
     
}

CompassIO::CompassIO(uint32_t baudRateIn,bytesize_t byteSizeIn, string portIn, string dataIn, Timeout timeoutIn, parity_t parityIn,stopbits_t stopBitsIn, flowcontrol_t flowControlIn, size_t sizeIn,string eolIn, string dataInInput)
{
        baudRate = baudRateIn;
        byteSize = byteSizeIn;
        port = portIn;
        data = dataIn;
        timeout = timeoutIn;
        parity = parityIn;
        stopBits = stopBitsIn;
        flowControl = flowControlIn;
        size = sizeIn;
        eol = eolIn;
        dataIn = dataInInput;   
        serial::Serial myCompass(port, baudRate, timeout,byteSize, parity,stopBits,flowControl);   

}


void CompassIO::read(){ 
    serial::Serial myCompass(port, baudRate, timeout,byteSize, parity,stopBits,flowControl);
    
   
    //port, baudrate, timeout, bytesize, parity, stopbits, flowcontrol
    if(!myCompass.isOpen())
    {
        cout << "The port is not open, opening...";
        myCompass.open();

    }
    dataIn = myCompass.readline(size,eol);
    size_t length = dataIn.length()-1;
    size_t start = 0;
    size_t end = 0;
    compassData.clear();
    while(end < length)
    {
        end = dataIn.find_first_of(delim, start);
        if(end < length)
        {
        compassData.push_back(dataIn.substr(start,end-start));
        }
        start = end+1;
    }
    int lastDelim = dataIn.find_last_of(delim, length);
    compassData.push_back(dataIn.substr(lastDelim+1,dataIn.length()-lastDelim));  
}

 vector<string> CompassIO::getCompassData()
{
    return(compassData);
}

 string CompassIO::getDataReadIn()
{
    return(dataIn);
}

 double CompassIO::getHeadingNum()
{
    return(atof(compassData[1].c_str()));
}

 string CompassIO::getHeadingString()
{
    return(compassData[1]);
}

 string CompassIO::getMagnStatus()
{
    return(compassData[2]);
}

 string CompassIO::getMagnStatusText()
{
    return(statusCheck(compassData[2]));
}

 double CompassIO::getPitchNum()
{
    return(atof(compassData[3].c_str()));
}

 string CompassIO::getPitchString()
{
    return(compassData[3]);
}

 string CompassIO::getPitchStatus()
{
    return(compassData[4]);
}

 string CompassIO::getPitchStatusText()
{
    return(statusCheck(compassData[4]));
}

 double CompassIO::getRollNum()
{
    return(atof(compassData[5].c_str()));
}

 string CompassIO::getRollString()
{
    return(compassData[5]);
}

 string CompassIO::getRollStatus()
{
    return(compassData[6]);
}

 string CompassIO::getRollStatusText()
{
    return(statusCheck(compassData[6]));
}

 double CompassIO::getDipNum()
{
    return(atof(compassData[7].c_str()));
}

 string CompassIO::getDipString()
{
    return(compassData[7]);
}


//Returns the relative magnitude horizontal component of earth’s magnetic field
string CompassIO::getHorizMagn()
{
    return(compassData[8]);
}

string CompassIO::statusCheck(string letterIn)
{    
    char letter = letterIn[0];
    switch(letter)
    {
        case 'C':
        return("Magnetometer Calibration Alarm");
        break;
        case 'L':
        return("Low Alarm");
        break;
        case 'M':
        return("Low Warning");
        break;
        case 'N':
        return("Normal");
        break;
        case 'O':
        return("High Warning");
        break;
        case 'P':
        return("High Alarm");
        break;
        case 'H':
        return("Magnetometer Voltage Level Alarm");
        break;        
    }
}