#include "serial/serial.h"
#include <iostream>
#include <string>

using namespace std;
using namespace serial;

class CompassReader
{
    uint32_t baudRate;
    string port;
    string data;
    bytesize_t byteSize;
 	parity_t parity;
    stopbits_t stopBits;
	flowcontrol_t flowControl; 
    Timeout timeout;
    size_t size;
    string eol;
    string dataIn;
    double heading;
    string magStatus;
    double pitchAngle;
    string pitchStatus;
    double rollAngle;
    string rollStatus;
    double dipAngle;
    char delim;
    vector<string> compassData;
    serial::Serial myCompass;



    //Base constructor
    CompassReader()
    {
        baudRate = 19200;
        byteSize = eightbits;
        port = "/dev/ttyUSB0";
        data = "$PTNT,HTM*63";
        timeout = serial::Timeout::simpleTimeout(10000);
        parity = parity_none;
        stopBits = stopbits_one;
        flowControl = flowcontrol_hardware;
        size = 65536;
        eol = "\r\n";
        dataIn ="";
        delim = ',';
        serial::Serial myCompass(port, baudRate, timeout,byteSize, parity,stopBits,flowControl);

    }
    CompassReader(uint32_t baudRateIn,bytesize_t byteSizeIn, string portIn, string dataIn, Timeout timeoutIn, parity_t parityIn,stopbits_t stopBitsIn, flowcontrol_t flowControlIn, size_t sizeIn,string eolIn, string dataInInput)
    {
        baudRate = baudRateIn;
        byteSize = byteSizeIn;
        port = portIn;
        data = dataIn;
        timeout = timeoutIn;
        parity = parityIn;
        stopBits = stopBitsIn;
        flowControl = flowControlIn;
        size = sizeIn;
        eol = eolIn;
        dataIn = dataInInput;   
        serial::Serial myCompass(port, baudRate, timeout,byteSize, parity,stopBits,flowControl);   

    }

    //$PTNTHTM,99.9,N,0.4,N,-0.2,N,58.8,6446*26
    void read()
    { 
    //port, baudrate, timeout, bytesize, parity, stopbits, flowcontrol
    dataIn = myCompass.readline(size,eol);
    size_t length = dataIn.length()-1;
    size_t start = 0;
    size_t end = 0;
    compassData.clear();

    while(end < length)
    {
        end = dataIn.find_first_of(delim, start);
        if(end < length)
        {
        compassData.push_back(dataIn.substr(start,end));
        }
        start = end+1;
    }
      compassData.push_back(dataIn.substr(dataIn.find_last_of(delim, length),length));  
    }

    public: vector<string> getCompassData()
    {
        return(compassData);
    }

    public: string getDataReadIn()
    {
        return(dataIn);
    }

    public: double getHeadingNum()
    {
        return(atof(compassData[1].c_str));
    }

    public: string getHeadingString()
    {
        return(compassData[1]);
    }

    public: string getMagnStatus()
    {
        return(compassData[2]);
    }

    public: string getMagnStatusText()
    {
        return(statusCheck(compassData[2]));
    }

    public: double getPitchNum()
    {
        return(atof(compassData[3].c_str));
    }

    public: string getPitchString()
    {
        return(compassData[3]);
    }

    public: string getPitchStatus()
    {
        return(compassData[4]);
    }

    public: string getPitchStatusText()
    {
        return(statusCheck(compassData[4]));
    }

    public: double getRollNum()
    {
        return(atof(compassData[5].c_str));
    }

    public: string getRollString()
    {
        return(compassData[5]);
    }

    public: string getRollStatus()
    {
        return(compassData[6]);
    }

    public: string getRollStatusText()
    {
        return(statusCheck(compassData[6]));
    }

    public: double getDipNum()
    {
        return(atof(compassData[7].c_str));
    }

    public: string getDipString()
    {
        return(compassData[7]);
    }

    public: string getDipStatus()
    {
        return(compassData[8]);
    }

    public: string getDipStatusText()
    {
        return(statusCheck(compassData[8]));
    }

    //Returns the relative magnitude horizontal component of earth’s magnetic field
    public: string getHorizMagn()
    {
        return(compassData[9]);
    }

    public: string statusCheck(string letterIn)
    {
        char letter = letterIn.c_str;
        switch(letter)
        {
            case 'C':
            return("Magnetometer Calibration Alarm");
            break;

            case 'L':
            return("Low Alarm");
            break;

            case 'M':
            return("Low Warning");
            break;

            case 'N':
            return("Normal");
            break;

            case 'O':
            return("High Warning");
            break;

            case 'P':
            return("High Alarm");
            break;

            case 'H':
            return("Magnetometer Voltage Level Alarm");
            break;        
        }
    }

};



