import serial
ser = serial.Serial('/dev/ttyS0', 19200, timeout=1)
# for x in range(10):
#Simple test script to read and parse data from a GPS (GlobalSat BU-353S4) and display it through the terminal
#Written By: Nicholas Wilkie
#Version: 2020-03-07
while 1:
    line = ser.readline()
    splitline = line.split(',')

    if splitline[0] == '$GPGGA':
        time = splitline[1]
        latitiude = splitline[2]
        latDirec = splitline[3]
        longitude = splitline[4]
        longDirec = splitline[5]
        satelliteNum = splitline[7]
        altitude = splitline[9]
        print("Time:",time)
        print("Latitude:", latitiude, latDirec)
        print("Longitude:", longitude, longDirec)
        print("Altitude:", altitude)
        print("Number of Satellites:", satelliteNum)
        print(line)
        break
