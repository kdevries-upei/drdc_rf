#include "serial/serial.h"
#include <iostream>
#include <string>
#include <fstream>

using namespace std;
using namespace serial;

int main()
{
    uint32_t baudrate = 19200;
    string output = "$PTNT,HTM*63";
    const std::string &port = "/dev/ttyUSB0";
    const std::string &data = "$PTNT,HTM*63\r\n";
    bytesize_t bytesize = eightbits;
    parity_t parity = parity_none;
    stopbits_t stopbits = stopbits_one;
    flowcontrol_t flowcontrol = flowcontrol_none;
    Timeout timeout = Timeout();
    size_t dataWrite;

    serial::Serial myCompass2(port, baudrate, timeout, bytesize, parity, stopbits, flowcontrol);

    cout << "\nWrite Data!\n";
    dataWrite = myCompass2.write(data);
    cout << dataWrite;
    cout << "\n";
}
