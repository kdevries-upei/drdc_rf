        #include <sys/types.h>
        #include <sys/stat.h>
        #include <fcntl.h>
        #include <termios.h>
        #include <stdio.h>
        #include <unistd.h> /* UNIX Standard Definitions         */
        #include <errno.h> 
        #include <strings.h>
        #include <string.h>
        #include <iostream>

        using namespace std;
        /* baudrate settings are defined in <asm/termbits.h>, which is
        included by <termios.h> */
        #define BAUDRATE B19200            
        /* change this definition for the correct port */
        #define MODEMDEVICE "/dev/ttyUSB0"
        //#define _POSIX_SOURCE 1 /* POSIX compliant source */

        #define FALSE 0
        #define TRUE 1

        volatile int STOP=FALSE; 

        int main()
        {
          string output = "$PTNT,HTM*63";
          int fd,c,res;
          struct termios oldtio,newtio;
          char buf[13] = "$PTNT,HTM*63";
        /* 
          Open modem device for reading and writing and not as controlling tty
          because we don't want to get killed if linenoise sends CTRL-C.
        */
         fd = open(MODEMDEVICE, O_RDWR | O_NOCTTY ); 
         if (fd < 0) 
         {
             perror(MODEMDEVICE); _exit(-1); 
            cout << "serial not open";
         }
        
         tcgetattr(fd,&oldtio); /* save current serial port settings */
         bzero(&newtio, sizeof(newtio)); /* clear struct for new port settings */        
        
         newtio.c_cflag = BAUDRATE | CS8 | CLOCAL | CREAD;
         newtio.c_iflag = ICRNL;
         newtio.c_oflag = 0;
         newtio.c_lflag = ICANON;
        
         newtio.c_cc[VINTR]    = 0;     /* Ctrl-c */ 
         newtio.c_cc[VQUIT]    = 0;     /* Ctrl-\ */
         newtio.c_cc[VERASE]   = 0;     /* del */
         newtio.c_cc[VKILL]    = 0;     /* @ */
         newtio.c_cc[VEOF]     = 4;     /* Ctrl-d */
         newtio.c_cc[VTIME]    = 0;     /* inter-character timer unused */
         newtio.c_cc[VMIN]     = 1;     /* blocking read until 1 character arrives */
         newtio.c_cc[VSWTC]    = 0;     /* '\0' */
         newtio.c_cc[VSTART]   = 0;     /* Ctrl-q */ 
         newtio.c_cc[VSTOP]    = 0;     /* Ctrl-s */
         newtio.c_cc[VSUSP]    = 0;     /* Ctrl-z */
         newtio.c_cc[VEOL]     = 0;     /* '\0' */
         newtio.c_cc[VREPRINT] = 0;     /* Ctrl-r */
         newtio.c_cc[VDISCARD] = 0;     /* Ctrl-u */
         newtio.c_cc[VWERASE]  = 0;     /* Ctrl-w */
         newtio.c_cc[VLNEXT]   = 0;     /* Ctrl-v */
         newtio.c_cc[VEOL2]    = 0;     /* '\0' */        


         tcflush(fd, TCIFLUSH);
         tcsetattr(fd,TCSANOW,&newtio);
        
            res = write(fd,buf,13);
            cout << res;
         
         /* restore the old port settings */
        tcsetattr(fd,TCSANOW,&oldtio);

        system("echo $PTNT,HTM*63 < /dev/ttyUSB0");
        }
