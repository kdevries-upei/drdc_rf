#include "serial/serial.h"
#include <iostream>
#include <string>

using namespace std;
using namespace serial;


int main()
{
    uint32_t baudrate = 19200;
    const std::string &port = "/dev/ttyUSB0";
    bytesize_t bytesize = eightbits;
 	parity_t parity = parity_none;
    stopbits_t stopbits = stopbits_one;
	flowcontrol_t flowcontrol = flowcontrol_software; 
    //Timeout timeout = Timeout();
    size_t size = 65536;
    string delim = "\n";
    

//port, baudrate, timeout, bytesize, parity, stopbits, flowcontrol
serial::Serial myCompass(port, baudrate, serial::Timeout::simpleTimeout(10000),bytesize, parity,stopbits,flowcontrol);
string lineIn = myCompass.readline(size,"\n");
cout << lineIn;
}


