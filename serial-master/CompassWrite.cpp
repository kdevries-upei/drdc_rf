#include "serial/serial.h"
#include <iostream>
#include <string>

using namespace std;
using namespace serial;


int main()
{
    uint32_t baudrate = 19200;
    const std::string & port = "/dev/ttyUSB0";
    string data = "$PTNT,HTM*63";
    bytesize_t bytesize = eightbits;
 	parity_t parity = parity_none;
    stopbits_t stopbits = stopbits_one;
	flowcontrol_t flowcontrol = flowcontrol_hardware; 
    //Timeout timeout = Timeout();     
    size_t dataWrite;   
    

//port, baudrate, timeout, bytesize, parity, stopbits, flowcontrol
serial::Serial myCompass2(port, baudrate, serial::Timeout::simpleTimeout(1000),bytesize, parity,stopbits,flowcontrol);
if(myCompass2.isOpen())
{
cout << "\nIt's open!";

}
cout << "\nWrite Data!\n";
dataWrite = myCompass2.write(data);
cout << dataWrite;
cout << "\n";
}


