/**
 * Developed for the DRDC Radio Frequency Measurement System.
 * This product includes software developed by William Woodall.
 * 
 * Purpose: Tests the functions implemented in gpsIO.cpp.
 * 
 *  @author Nicholas Wilkie
 *  @version 2020-03-31
 */
#include "gpsIO.h"

int main()
{
  gpsIO gpsOne = gpsIO();
  gpsOne.readUntilFix();

  cout << "\nThe data in is: " + gpsOne.getDataReadIn();
  cout << "\nThe time number is: " << gpsOne.getTimeNum();
  cout << "\nThe Time string is: " + gpsOne.getTimeString();
  cout << "\nThe latitude num is: " << gpsOne.getLatitudeNum();
  cout << "\nThe latititude string is " + gpsOne.getLatitudeString();
  cout << "\nThe latititude direction is " + gpsOne.getLatDirec();
  cout << "\nThe longitude num is: " << gpsOne.getLongitudeNum();
  cout << "\nThe longitude string is: " + gpsOne.getLongitudeString();
  cout << "\nThe longitude direction is: " + gpsOne.getLonDirec();
  cout << "\nThe fix number is: " << gpsOne.getFixNum();
  cout << "\nThe fix string is: " + gpsOne.getFixString();
  cout << "\nThe satellite number is: " << gpsOne.getSatelliteNum();
  cout << "\nThe altitude number is: " << gpsOne.getAltitudeNum();
  cout << "\nThe altitude string is: " << gpsOne.getAltitudeString();
  cout << "\nThe horizontal Dilution Num is: " << gpsOne.getHorDilutionNum();
  cout << "\nThe horizontal Dilution string is: " << gpsOne.getHorDilutionString();
  cout << "\nThe Geoid height num is: " << gpsOne.geoidHeighNum();
  cout << "\nThe Geoid height string is " << gpsOne.geoidHeightString();
  cout << "\nThe checksum data is " + gpsOne.checkSumData() + "\n";
}
