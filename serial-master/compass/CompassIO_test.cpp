/**
 * Developed for the DRDC Radio Frequency Measurement System.
 * This product includes software developed by William Woodall.
 * 
 * Purpose: Tests the functions implemented in compassIO.cpp.
 * 
 *  @author Nicholas Wilkie
 *  @version 2020-03-30
 */
#include "CompassIO.h"
#include <thread>
#include <chrono>

using namespace std::this_thread; // sleep_for, sleep_until

int main()
{
    CompassIO compassOne = CompassIO(); //declares compassIO object
    compassOne.read();
    //std::thread t1(&CompassIO::read, CompassIO());
    //sleep_for(1000ms);
    //std::thread t2(&CompassIO::write, CompassIO());
    ////port, baudrate, timeout, bytesize, parity, stopbits, flowcontrol
    //t1.join();
    //t2.join();

    cout << "\nThe data in is:" + compassOne.getDataReadIn();
    cout << "\nHeading Number: " << compassOne.getHeadingNum();
    cout << "\nThe Heading is: " + compassOne.getHeadingString();

    cout << "\nMagnitude Status: " + compassOne.getMagnStatus();
    cout << "\nMagnitude Status: " + compassOne.getMagnStatusText();

    cout << "\nPitch Number: " << compassOne.getPitchNum();
    cout << "\nThe Pitch is: " + compassOne.getPitchString();
    cout << "\nThe Pitch Status is: " + compassOne.getPitchStatus();
    cout << "\nThe Pitch Status is: " + compassOne.getPitchStatusText();

    cout << "\nRoll Number: " << compassOne.getRollNum();
    cout << "\nThe roll is: " + compassOne.getRollString();
    cout << "\nRoll Status: " + compassOne.getRollStatus();
    cout << "\nRoll Status: " + compassOne.getRollStatusText();

    cout << "\nDip Number: " << compassOne.getDipNum();
    cout << "\nThe Dip is: " + compassOne.getDipString();

    cout << "\nThe Horizontal Magnitude: " + compassOne.getHorizMagn() + "\n";
}
