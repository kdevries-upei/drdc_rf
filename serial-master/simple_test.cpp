#include "serial/serial.h"
#include <iostream>
#include <string>

using namespace std;
using namespace serial;


int main()
{
    uint32_t baudrate = 4800;
    const std::string &port = "/dev/ttyUSB0";
    bytesize_t bytesize = eightbits;
 	parity_t parity = parity_none;
    stopbits_t stopbits = stopbits_one;
	flowcontrol_t flowcontrol = flowcontrol_none; 
    //Timeout timeout = Timeout();
    size_t size = 65536;
    string delim = "\n";
    

//port, baudrate, timeout, bytesize, parity, stopbits, flowcontrol
serial::Serial myGPS(port, baudrate, serial::Timeout::simpleTimeout(1000),bytesize, parity,stopbits,flowcontrol);
string lineIn = myGPS.readline(size,"\n");
cout << lineIn;
myGPS.close();
}


